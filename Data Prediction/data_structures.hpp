//
//  data_structures.h
//  Data Prediction (project)
//
//  Created by Ever on 21/5/18.
//  Copyright © 2018 Ever. All rights reserved.
//

#ifndef data_structures_hpp
#define data_structures_hpp

#include <vector>
#include <tuple>
#include "opencv2/imgproc.hpp"

/*  Both historicalDataSet and crowdedness_predictions are GLOBAL variables declared extern in this header file and defined in their single respective source code file. They're meant to be accessible by the whole project. */

typedef struct
{
    unsigned int ID;
    unsigned int Has_darts;
    unsigned int Has_football;
    unsigned int Has_pool;
    
    float Beer_price;
    float Cafe_price;
    float Soda_price;
    double Longitude;
    double Latitude;
    
    unsigned int Beer_ometer;
    unsigned int Atmosphere;
    
    std::vector< double > morning, midday, evening, late_night;
} BarData;

extern std::vector< BarData > historicalDataSet;

typedef struct
{
    unsigned int rating;
    unsigned int atmosphere;
    unsigned int football;
    
    std::string neighborhood;
    std::string name;
    
    unsigned int zone;
    
    float beerPrice;
    
    unsigned int billiards;
    unsigned int prediction;
    
    double longitude;
    
    float beerPrice_prediction;
    
    double latitude;
    
    float sodaPrice;
    
    std::string url;
    
    unsigned int darts;
    
    float coffeePrice;
    
    unsigned int prediction_rating;
    
    float beerPrice_prediction_rating;
    float beerPrice_rating;
    
    unsigned int id;
} processedData;

extern std::vector< processedData > processedDataSet;


#endif /* data_structures_hpp */
