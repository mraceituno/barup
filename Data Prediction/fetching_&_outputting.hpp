//
//  fetching & outputting.hpp
//  Data Prediction (project)
//
//  Created by Ever on 20/5/18.
//  Copyright © 2018 Ever. All rights reserved.
//

#ifndef fetching___outputting_hpp
#define fetching___outputting_hpp

#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "data_structures.hpp"
#include <string>
#include <omp.h>

// Auxiliary subroutine to parse .JSON files data
template< typename T >
T parse_value(const std::__1::basic_string< char > * value);

// Main subroutine that populates historicalDataSet from a root of the indicated type
int fill_data(const boost::property_tree::ptree & root, const bool & VERBOSE, const bool & PARALLEL, const unsigned int & bars);

int fill_processed_data(const boost::property_tree::ptree & root, const bool & VERBOSE, const bool & PARALLEL, const unsigned int & bars);

// Auxiliary subroutine for outputting data in an output .JSON file
boost::property_tree::ptree fill_node_predictions(const std::string & s, const std::vector< cv::Point2d > & v);

// Output predictions on a .JSON file
int output_predictions(boost::property_tree::ptree & output, const bool & PARALLEL, const bool & VERBOSE, const unsigned int & bars);

// Template subroutine for filling the structure with the data fetched from the .JSON file
template< typename T >
int fetch_data(const std::__1::basic_string< char > & value, const std::string & name, T * field);

// Template specialization for the case where the structure's field is a vector (note that the behavior is different)
template<>
int fetch_data(const std::__1::basic_string< char > & value, const std::string & name, std::vector< double > * vec);

#endif /* fetching___outputting_hpp */
