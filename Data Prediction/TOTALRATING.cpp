//
//  TOTALRATING.cpp
//  Distributed Project
//
//  Created by Arnau Oller on 27/05/2018.
//  Copyright © 2018 Arnau Oller. All rights reserved.
//

#include "TOTALRATING.hpp"

/* The idea is that using attributes of each bar and the crowdnes they have, to predict if a bar with some characteristic will be good or not
 the SVM will separate 3 classes*/

#define FEATURES 10 // we add the day as a feature and don't take into account the ID as a feature

//ARREGLAR
using namespace cv;
using namespace cv::ml;

//ARREGLAR
int generalPredictionSvm(const int & numBars,const int & darts,const int & football, const int & pool, const float &  beer, const float & cafe,const float & soda,const float & longitude,const float & latitude,const int & ometer,const int & Atmos)
{
    /*------include this in the wiki!!*/
    //************UYTFDE(&%R&/)T/()=
    
    // timeframes: 0 - morning; 1 - midday; 2 - evening; 3 - night;
    // barID: 1,2,...
    // weekday: 0,1,2,3,4,5,6
    //numBar: How many bars from the dataset do you want to use

    
    
    
    
   //Check if the INPUT values are correct
    checkInput(numBars, darts, football, pool, beer, cafe, soda, longitude, latitude, ometer, Atmos);
    
    // Set up training data
    int labels[numBars];
    float trainingData[numBars][FEATURES];
    
    //putting the FEATURES in the trainingData
    for(int j = 0;j <numBars;j++){
        // compute the "TOTAL CROWDNESS"
        float totalCrowd = 0;
        // add all the values the crowdness for a bar for all the timeframes and days as a single value of crowdness
        for (int i = 0; i > -180; i--)
        {
            
            float partialCrowd = historicalDataSet.at(j).morning.at(-i) +historicalDataSet.at(j).midday.at(-i)+historicalDataSet.at(j).evening.at(-i) + historicalDataSet.at(j).late_night.at(-i);
            totalCrowd = totalCrowd + partialCrowd;
            
        }
        
        printf("TOTAL CROWD is: %f \n",totalCrowd);
        //separate the corwdness in 3 lables: -1: bad, 0 good, 1 perfect
        if(totalCrowd <=240) labels[j] = {-1};
        else if(totalCrowd>240 && totalCrowd <= 480) labels[j] = {0};
            else if(totalCrowd>480) labels[j] = {1};
        
        trainingData[j][0] = (unsigned int)historicalDataSet.at( j ).Has_darts;
        trainingData[j][1] = (unsigned int)historicalDataSet.at( j ).Has_football;
        trainingData[j][2] = (unsigned int)historicalDataSet.at( j ).Has_pool;
        trainingData[j][3] = (float)historicalDataSet.at( j ).Beer_price;
        trainingData[j][4] = (float)historicalDataSet.at( j ).Cafe_price;
        trainingData[j][5] = (float)historicalDataSet.at( j ).Soda_price;
        trainingData[j][6] = (float)historicalDataSet.at( j ).Longitude;
        trainingData[j][7] = (float)historicalDataSet.at( j ).Latitude;
        trainingData[j][8] = (unsigned int)historicalDataSet.at( j ).Beer_ometer;
        trainingData[j][9] = (unsigned int)historicalDataSet.at( j ).Atmosphere;
    }

    
    Mat trainingDataMat(numBars, FEATURES, CV_32FC1, trainingData);
    Mat labelsMat(numBars, 1, CV_32SC1, labels);
    
    // Train the SVM
    Ptr<SVM> svm = SVM::create();
    svm->setType(SVM::C_SVC);
    svm->setKernel(SVM::LINEAR);
    svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 100, 1e-6));
    svm->train(trainingDataMat, ROW_SAMPLE, labelsMat);
    
    
    // put the parameters to predict if it they will be good or not
    float data[FEATURES];
    data[0] = darts;
    data[1] = football;
    data[2] = pool;
    data[3] = beer;
    data[4] = cafe;
    data[5] = soda;
    data[6] = longitude;
    data[7] = latitude;
    data[8] = ometer;
    data[9] = Atmos;

    //float data2[3] = {501,10,100};
    cv::Mat dataToPredict[FEATURES];
    dataToPredict[0] = cv::Mat(1, FEATURES, CV_32F, data);
  
    
    float response[0];
    response[0] = svm->predict(dataToPredict[0]);
   
    printf("\n------------\n");
    printf("for the predicted value: %f \n",response[0]);
    if (response[0] == -1)
        printf("It will be a failure!\n");
    else if (response[0] == 0)
        
        printf("Good, this is a good option, you will not be rich buy you will also not lose lot of money\n");
    else if (response[0] == 1)
        printf("You will make a lot of money! (it's only an advice)\n");
    
    printf("\n------------\n");
 
    return EXIT_SUCCESS;
}

//ARREGLAR
//it checks if the input values are correct
int checkInput (const int & numBars,const int & darts,const int & football, const int & pool, const float &  beer, const float & cafe,const float & soda,const float & longitude,const float & latitude,const int & ometer,const int & Atmos){
    
    
    if (numBars <= 0 )
    {
        std::cout << "You must specify a valid num of BARS. *(at least 1)* \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (darts < 0 )
    {
        std::cout << "You must specify a valid num of DARTS. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (football < 0 )
    {
        std::cout << "You must specify a valid FOOTBALL. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (pool < 0)
    {
        std::cout << "You must specify a valid POOL. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (beer < 0)
    {
        std::cout << "You must specify a valid BEER price. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (cafe < 0 )
    {
        std::cout << "You must specify a valid CAFE price. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (soda < 0 )
    {
        std::cout << "You must specify a valid SODA price. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (longitude < 0 )
    {
        std::cout << "You must specify a valid LONGITUDE value. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (latitude < 0 )
    {
        std::cout << "You must specify a valid LATITUDE value. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (ometer < 0 )
    {
        std::cout << "You must specify a valid OMETER value. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    if (Atmos < 0 )
    {
        std::cout << "You must specify a valid ATMOS value. \n" << std::endl;
        
        return EXIT_FAILURE;
    }
    
    return 0;
    
}
