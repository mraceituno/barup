//
//  SLR.hpp
//  Data Prediction (project)
//
//  Created by Ever on 20/5/18.
//  Copyright © 2018 Ever. All rights reserved.
//

#ifndef SLR_hpp
#define SLR_hpp

#include "fetching_&_outputting.hpp"
#include <limits>
#include <omp.h>

//The first time how many days we have of data
#define RECORDED_PAST_DAYS 20

#define MORNING 10
#define MIDDAY 11
#define EVENING 12
#define NIGHT 13

// It's a good idea to define those in the range [0,6] because we use these numbers in modular arithmetic calculations
#define MONDAY 0
#define TUESDAY 1
#define WEDNESDAY 2
#define THURSDAY 3
#define FRIDAY 4
#define SATURDAY 5
#define SUNDAY 6

/*  This subroutine is in charge of generating vector of points that will then be processed by the fitline() function from the OpenCV library. It generates the points in the format (day, level), where day referes to how far in the past the specific weekday was and level to the crowdedness level. For example, (0,0.2) would refer to the crowdedness level last monday. This is varies from bar to bar (hence the barID identification) and timeframe. */
int generate_input_array_slr(const int & weekday, const int & timeframe, const int & barID, std::vector< cv::Point2d > & vp,unsigned int recordedDays);

// This subroutine calls predict() as many times as necessary for the 28 estimations on each bar and populates crowdedness_predictions
int fill_crowdedness_predictions(const unsigned int & amount_predictions, const bool & VERBOSE, const bool & PARALLEL,unsigned int recordedDays,const int & choosenWeekday, const int & choosenTimeframe);

//arnau
//int fill_crowdedness_predictions(const unsigned int & amount_predictions, const bool & VERBOSE, const bool & PARALLEL, int timeFrameCommand, int dayCommand );


/*  Given the results of the regression make future predictions. It is a template because in case of 2D fitting, it should be a vector of 4 elements (like Vec4f) - (vx, vy, x0, y0), where (vx, vy) is a normalized vector collinear to the line and (x0, y0) is a point on the line and in case of 3D fitting, it should be a vector of 6 elements (like Vec6f) - (vx, vy, vz, x0, y0, z0), where (vx, vy, vz) is a normalized vector collinear to the line and (x0, y0, z0) is a point on the line. */
std::vector< cv::Point2d > predict(const cv::Vec4d & OutputArray, const unsigned int & amount_predictions);

#endif /* SLR_hpp */
