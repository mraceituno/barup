// aixo va dabutis

//
//  SLR.cpp
//  Data Prediction (project)
//
//  Created by Ever on 20/5/18.
//  Copyright © 2018 Ever. All rights reserved.
//

#include "SLR.hpp"


// check if the inputs of the function are correct
int is_valid_weekday(const int & d)
{
    return ( d >= MONDAY & d <= SUNDAY ) ? EXIT_SUCCESS : EXIT_FAILURE;
}

int is_valid_timeframe(const int & t)
{
    return ( t >= MORNING & t <= NIGHT ) ? EXIT_SUCCESS : EXIT_FAILURE;
}

// put all the data correctly inside the historicalDataSet
int generate_input_array_slr(const unsigned int & weekday, const unsigned int & timeframe, const unsigned int & barID, std::vector< cv::Point2d > & vp,unsigned int recordedDays)
{
    if ( is_valid_weekday(weekday) == EXIT_FAILURE )
    {
        std::cout << "You must specify a valid weekday." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    if ( is_valid_timeframe(timeframe) == EXIT_FAILURE )
    {
        std::cout << "You must specify a valid timeframe." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    //from the first time we make the prediction we keep getting more data each day
    
    int totalDaysRecorded = RECORDED_PAST_DAYS + recordedDays;
    
    for (int i = 0; i > -totalDaysRecorded; i--)
    {
        if ( -i % 7 == weekday )
        {
            if ( timeframe == MORNING )
                vp.push_back( cv::Point2d( (float) i,
                                          (float) historicalDataSet.at( barID - 1 ).morning.at(-i) ) );
            
            else if ( timeframe == MIDDAY )
                vp.push_back( cv::Point2d( (float) i,
                                          (float) historicalDataSet.at( barID - 1 ).midday.at(-i) ) );
            
            else if ( timeframe == EVENING )
                vp.push_back( cv::Point2d( (float) i,
                                          (float) historicalDataSet.at( barID - 1 ).evening.at(-i) ) );
            
            else if ( timeframe == NIGHT )
                vp.push_back( cv::Point2d( (float) i,
                                          (float) historicalDataSet.at( barID - 1 ).late_night.at(-i) ) );
            
            else { std::cout << "Invalid timeframe." << std::endl; return EXIT_FAILURE; }
        }
    }
    
    return EXIT_SUCCESS;
}

//since the cv::fitLine function returns a cv::Vec4d, with the function below we will make the prediction
std::vector< cv::Point2d > predict(const cv::Vec4d & OutputArray, const unsigned int & amount_predictions)
{
    std::vector< cv::Point2d > predictions;
    
    for (unsigned int i = 0; i < amount_predictions; i++)
    {
        // here we make the prediction:  (float) (OutputArray(1) * (i + 1) + OutputArray(3)) )
        predictions.push_back( cv::Point2d( (float) (i + 1), (float) (OutputArray(1) * (i + 1) + OutputArray(3)) ) );
    }
    
    return predictions;
}


//here we make the predictions and store them in the processedDataSet vector
int fill_crowdedness_predictions(const unsigned int & amount_predictions, const bool & VERBOSE, const bool & PARALLEL,unsigned int recordedDays,const int & choosenWeekday, const int & choosenTimeframe)
{
    if ( amount_predictions <= 0 )
    {
        std::cout << "You must specify a valid number of predictions to make." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    
    /*  We schedule the parallel loop dynamically because the linear regression calculation's complexity may vary depending on the data, and so it's unpredictable. */
    
#pragma omp parallel for schedule(dynamic) if (PARALLEL)
    
    for (unsigned int barID = 1; barID <= historicalDataSet.back().ID; barID++)
    {
        std::vector< cv::Point2d > vip;
        
        cv::Vec4d op;
        
        for (unsigned int timeframe = 0; timeframe <= 3; timeframe++)
        {
            for (unsigned int weekday = MONDAY; weekday <= SUNDAY; weekday++)
            {
                if ( generate_input_array_slr(weekday, timeframe + 10, barID, vip,recordedDays) == EXIT_FAILURE ) exit(EXIT_FAILURE);
                
                cv::fitLine(vip, op, cv::DIST_L2, 0, 0.01, 0.01);
                
                vip.clear();
                
                if(weekday == choosenWeekday){
                    if(timeframe == choosenTimeframe){
                        
                        // we take the second given by the predict function, is where the prediction is stored
                        float prediction = predict(op, amount_predictions)[0].y;
                        
                        unsigned int  result;
                        if(prediction <=0.33) result = 0;
                        else if (prediction >0.33 && prediction <=0.66) result = 1;
                        else result = 2;
                        
                        //DEBUG to know which values we have
                        
                         /*
                          std::cout <<"float prediction" <<prediction <<"\n";
                          std::cout <<"0,1,2 result:" << result <<"\n";
                          */
                        
                         
                        
                        
                        // if we don't do an atomical operation here the results will not be correct
#pragma omp critical
                        //store the result (0,1 or 2) in the corresponding bar
                        processedDataSet[barID].prediction = result;
                    }
                    
                    
                }
                
            }
            
            
        }
    }
    
    std::cout << std::endl;
    
    return EXIT_SUCCESS;
}
