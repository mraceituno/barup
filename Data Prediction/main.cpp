//
//  main.cpp
//  Data Prediction (project)
//
//  Created by Ever on 20/5/18.
//  Copyright © 2018 Ever. All rights reserved.
//

#include <chrono>
#include "SLR.hpp"
#include <omp.h>
// for how many days in advance we will preditc
#define AMOUNT_PREDICTIONS 1
//we will use 100 bars for the DEMO
#define BARS 100 // set this value accordingly, unfortunately it's a little difficult to make this work for all cases automatically
#define VERBOSE false // set to true/false depending on whether you want to see A LOT of output on run-time, useful for debugging
#define PARALLEL true // set to true/false depending on whether you want to run the code parallely

int main(int argc, const char * argv[])
{
    // wich timeframe --> from 0 to 3
    const int timeFrameCommand = atoi(argv[1]);
    //which day of the week --> from 0 to 6
    const int dayCommand = atoi(argv[2]);
    
    //which day since the first predicion we made, the first day of a prediction is 0, the second one 1 and so on
    // --> from 0 to (total number of bars -20)
    const int todayDay = atoi(argv[3]);
    
    boost::property_tree::ptree root, processedDataTree, output;
    
    std::chrono::duration< double, std::ratio<1,1> > elapsed_seconds1, elapsed_seconds2, elapsed_seconds3, elapsed_seconds4, elapsed_seconds5;
    
    // STEP 1: Read the data from the .JSON file.
    // This section of the code is sequential.
    
    auto start = std::chrono::system_clock::now();
    
    // in the 2 following lines is important to have the 2 JSON files with the same number of bars
    //this is the data set with crowdedness values
    boost::property_tree::read_json( "BarData_Prediction_100.JSON", root );
    //this is the JSON needed by the app
    boost::property_tree::read_json( "firebaseData.json", processedDataTree );
    
    
    auto end = std::chrono::system_clock::now();
    
    elapsed_seconds1 = end - start;
    
    std::cout << "Time taken fetching the data from the .JSON file:\t\t\t" << elapsed_seconds1.count() << " (s)\n";
    
    if (VERBOSE) std::cout << "\n\n\n";
    
    // STEP 2: Store this data in our program.
    // This section of the code is parallelizable.
    
    start = std::chrono::system_clock::now();
    //this is the data set with crowdedness values
    if ( fill_data( root, VERBOSE, PARALLEL, BARS ) == EXIT_FAILURE ) exit(EXIT_FAILURE);
    //this is the JSON needed by the app
    if ( fill_processed_data( processedDataTree, VERBOSE, PARALLEL, BARS ) == EXIT_FAILURE ) exit(EXIT_FAILURE);
    
    end = std::chrono::system_clock::now();
    
    elapsed_seconds2 = end - start;
    
    if (VERBOSE) std::cout << "\n\n";
    
    std::cout << "Time taken saving the data in the structures:\t\t\t\t" << elapsed_seconds2.count() << " (s)\n";
    
    if (VERBOSE) std::cout << "\n\n\n";
    
    // STEP 3: Make the predictions and save the results in our program.
    // This section of the code is parallelizable.
    
    start = std::chrono::system_clock::now();
    
    if ( fill_crowdedness_predictions( AMOUNT_PREDICTIONS, VERBOSE, PARALLEL,todayDay,dayCommand,timeFrameCommand) == EXIT_FAILURE ) exit(EXIT_FAILURE);
    
    end = std::chrono::system_clock::now();
    
    elapsed_seconds3 = end - start;
    
    if (VERBOSE) std::cout << "\n\n";
    
    std::cout << "Time taken making the predictions:\t\t\t\t\t\t\t" << elapsed_seconds3.count() << " (s)\n\n";
    
    // STEP 4: Fill up an output node with the predictions in order to be able to generate a .JSON file from it.
    // This section of the code is parallelizable.
    
    start = std::chrono::system_clock::now();
    
    if ( output_predictions( output, PARALLEL, VERBOSE, BARS ) == EXIT_FAILURE ) exit(EXIT_FAILURE);
    
    end = std::chrono::system_clock::now();
    
    elapsed_seconds4 = end - start;
    
    std::cout << "Time taken filling up an output node with the predictions:\t" << elapsed_seconds4.count() << " (s)\n\n";
    
    // STEP 5: Output the predictions to a .JSON file.
    // This section of the code is sequential.
    
    start = std::chrono::system_clock::now();
    
    boost::property_tree::write_json( "output.json", output );
    
    end = std::chrono::system_clock::now();
    
    elapsed_seconds5 = end - start;
    
    std::cout << "Time taken outputting the predictions to a .JSON file:\t\t" << elapsed_seconds5.count() << " (s)\n\n";
    
    // Total execution timings.
    
    std::cout << "Total execution time of the main components of the program: "
    << elapsed_seconds1.count() + elapsed_seconds2.count() + elapsed_seconds3.count() +
    elapsed_seconds4.count() + elapsed_seconds5.count() << " (s)\n" << std::endl;
    
    return EXIT_SUCCESS;
}
