//
//  SVM.cpp
//  Distributed Project
//
//  Created by Arnau Oller on 27/05/2018.
//  Copyright © 2018 Arnau Oller. All rights reserved.
//

#include "SVM.hpp"
#define FEATURES 11 // we add the day as a feature and don't take into account the ID as a feature
//#define DAYS 26 // from how many days we have data
#define SIZEDATA 180


using namespace cv;
using namespace cv::ml;


int generate_input_array_svm(const int & weekday, const int & timeframe, const int & barID,const int & DAYSTOPREDICT)
{
    // check if the input values are correct
    checkInput(weekday, timeframe, barID, DAYSTOPREDICT);
    
    int DAYS = SIZEDATA/7+1; /// from how many days we have data, for example if we choose mondays, howm many mondays we have
    
    // Set up training data
    float labels[DAYS];
    float trainingData[DAYS][FEATURES];
   
    //putting the FEATURES in the trainingData
    int counter = 0; // used to check how many days we will use for our SVM
    for (int i = 0; i > -SIZEDATA; i--)
    {
        
        
        if ( -i % 7 == weekday )
        {
            // We put the values of the crowdedness in every timeframe and days inside labels with the same proportion as they had before buy the possible values for labels are -1,0 and 1
            if ( timeframe == 0 ){
                if(historicalDataSet.at( barID - 1 ).morning.at(counter)<=0.3333) labels[counter] = -1 ;
                else if(historicalDataSet.at( barID - 1 ).morning.at(counter)>0.3333 && historicalDataSet.at( barID - 1 ).morning.at(counter)<=0.6666) labels[counter] = 0  ;
                else if(historicalDataSet.at( barID - 1 ).morning.at(counter) >0.6666) labels[counter]=1;
                
                counter++;
            }
            
            else if ( timeframe == 1 ){
                if(historicalDataSet.at( barID - 1 ).midday.at(counter)<=0.3333) labels[counter] = -1 ;
                else if(historicalDataSet.at( barID - 1 ).midday.at(counter)>0.3333 && historicalDataSet.at( barID - 1 ).morning.at(counter)<=0.6666) labels[counter] = 0  ;
                else if(historicalDataSet.at( barID - 1 ).midday.at(counter) >0.6666) labels[counter]=1;
                
                counter++;
            }
            
            else if ( timeframe == 2 ){
                if(historicalDataSet.at( barID - 1 ).evening.at(counter)<=0.3333) labels[counter] = -1 ;
                else if(historicalDataSet.at( barID - 1 ).evening.at(counter)>0.3333 && historicalDataSet.at( barID - 1 ).morning.at(counter)<=0.6666) labels[counter] = 0  ;
                else if(historicalDataSet.at( barID - 1 ).evening.at(counter) >0.6666) labels[counter]=1;
                
                counter++;
            }
            
            else if ( timeframe == 3 ){
                if(historicalDataSet.at( barID -1 ).late_night.at(counter)<=0.3333) labels[counter] = -1 ;
                else if(historicalDataSet.at( barID  -1).late_night.at(counter)>0.3333 && historicalDataSet.at( barID - 1 ).morning.at(counter)<=0.6666) labels[counter] = 0;
                else if(historicalDataSet.at( barID -1 ).late_night.at(counter) >0.6666) labels[counter]=1;
                
                counter++;
            }
            
            trainingData[counter][0] = counter; // here we put the day of the sample
            trainingData[counter][1] = (unsigned int)historicalDataSet.at( barID - 1 ).Has_darts;
            trainingData[counter][2] = (unsigned int)historicalDataSet.at( barID - 1 ).Has_football;
            trainingData[counter][3] = (unsigned int)historicalDataSet.at( barID - 1 ).Has_pool;
            trainingData[counter][4] = (float)historicalDataSet.at( barID - 1 ).Beer_price;
            trainingData[counter][5] = (float)historicalDataSet.at( barID - 1 ).Cafe_price;
            trainingData[counter][6] = (float)historicalDataSet.at( barID - 1 ).Soda_price;
            trainingData[counter][7] = (float)historicalDataSet.at( barID - 1 ).Longitude;
            trainingData[counter][8] = (float)historicalDataSet.at( barID - 1 ).Latitude;
            trainingData[counter][9] = (unsigned int)historicalDataSet.at( barID - 1 ).Beer_ometer;
            trainingData[counter][10] = (unsigned int)historicalDataSet.at( barID - 1 ).Atmosphere;
        }
    }
    
    printf("\nTHE TOTAL VALUE OF THE COUNTER IS : %d\n\n",counter);

    cv::Mat trainingDataMat(counter, FEATURES, CV_32FC1, trainingData);
    Mat labelsMat(counter, 1, CV_32SC1, labels);
    
    // Train the SVM
    Ptr<SVM> svm = SVM::create();
    svm->setType(SVM::C_SVC);
    svm->setKernel(SVM::LINEAR);
    svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 100, 1e-6));
    svm->train(trainingDataMat, ROW_SAMPLE, labelsMat);
    
    
    // Fill the data we want to PREDICT
    float data[DAYSTOPREDICT][FEATURES];
    float day = 0;
    for(int i = 0; i < DAYSTOPREDICT;i++){
        
        day = counter+1+i;
        
        data[i][0] = day;
        data[i][1]= trainingData[0][1];
        data[i][2] = trainingData[0][2];
        data[i][3] = trainingData[0][3];
        data[i][4] = trainingData[0][4];
        data[i][5] = trainingData[0][5];
        data[i][6] = trainingData[0][6];
        data[i][7] = trainingData[0][7];
        data[i][8] = trainingData[0][8];
        data[i][9] = trainingData[0][9];
        data[i][10] = trainingData[0][10];
        
    }
    // Put in a good format the data we want to predict and make the PREDICTION
    //the cv::Mat representa a n-dimensional dense numerical channel array
    cv::Mat dataToPredict[DAYSTOPREDICT];
    //here we will store the predictions
    float response[DAYSTOPREDICT];
    
    //As data to predict we put the features and also the bar we want to predict
    for(int i = 0; i<DAYSTOPREDICT;i++){
        dataToPredict[i] = cv::Mat(1, FEATURES, CV_32F, data[i]);
        response[i] = svm->predict(dataToPredict[i]);
    }
    
    /* prints to se the OUTPUT
     
    for(int i = 0; i<DAYSTOPREDICT;i++){
        
        printf("The response value is : %f \n",response[i]);
        if (response[i] == -1)
            printf("It will be EMPTY, there are better places to go!\n");
        else if (response[i] == 0)
            printf("It will be HALF FULL, it's a good place to go\n");
        else if (response[i] == 1)
            printf("It will be FULL, maybe you will not have many space but it will be GREAT!\n");
    }
    
     */
    
    return EXIT_SUCCESS;
}

/*

int output_predictions(boost::property_tree::ptree & SVMoutput,const int & weekday, const int & timeframe, const int & barID,const int & DAYSTOPREDICT)
{
    
    boost::property_tree::ptree node;
    
    for (int i = 0; i<1;i++)
    {
        std::ostringstream s;
        
        node.put("ID", 1);
        
        s << "12";
        
        node.add_child("y_morning_monday",  "ad");
        node.add_child("y_morning_tuesday", "jha"));
        
        SVMoutput.add_child(s.str(), node);
    }


}

 */
/*
int fill_crowdedness_predictions(const unsigned int & amount_predictions)
{
    if (amount_predictions <= 0)
    {
        std::cout << "You must specify a valid number of predictions to make." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    std::vector< cv::Point2f > vip;
    
    cv::Vec4f op;
    
    //    int f = 0;
    
    for (unsigned int barIDs = 1; barIDs <= historicalDataSet.back().ID; barIDs++)
    {
        SVM_prediction cp;
        
        for (unsigned int timeframes = 0; timeframes < 4; timeframes++)
        {
            for (unsigned int weekdays = 0; weekdays < 7; weekdays++)
            {
                
               
                
                cp.ID = barIDs;
                
                if ( timeframes == 0 & weekdays == 0 ) cp.y_morning_monday = generate_input_array_svm(weekdays,timeframes,barIDs,1);
                else if ( timeframe == 0 & weekday == 1 ) cp.y_morning_tuesday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 0 & weekday == 2 ) cp.y_morning_wednesday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 0 & weekday == 3 ) cp.y_morning_thursday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 0 & weekday == 4 ) cp.y_morning_friday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 0 & weekday == 5 ) cp.y_morning_saturday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 0 & weekday == 6 ) cp.y_morning_sunday = generate_input_array_svm(weekday,timeframe,barID,1);
                
                else if ( timeframe == 1 & weekday == 0 ) cp.y_midday_monday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 1 & weekday == 1 ) cp.y_midday_tuesday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 1 & weekday == 2 ) cp.y_midday_wednesday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 1 & weekday == 3 ) cp.y_midday_thursday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 1 & weekday == 4 ) cp.y_midday_friday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 1 & weekday == 5 ) cp.y_midday_saturday = generate_input_array_svm(weekday,timeframe,barID,1);
                else if ( timeframe == 1 & weekday == 6 ) cp.y_midday_sunday = generate_input_array_svm(weekday,timeframe,barID,1);
                
                else if ( timeframe == 2 & weekday == 0 ) cp.y_evening_monday = predict(op, amount_predictions);
                else if ( timeframe == 2 & weekday == 1 ) cp.y_evening_tuesday = predict(op, amount_predictions);
                else if ( timeframe == 2 & weekday == 2 ) cp.y_evening_wednesday = predict(op, amount_predictions);
                else if ( timeframe == 2 & weekday == 3 ) cp.y_evening_thursday = predict(op, amount_predictions);
                else if ( timeframe == 2 & weekday == 4 ) cp.y_evening_friday = predict(op, amount_predictions);
                
                else if ( timeframe == 2 & weekday == 5 ) cp.y_evening_saturday = predict(op, amount_predictions);
                else if ( timeframe == 2 & weekday == 6 ) cp.y_evening_sunday = predict(op, amount_predictions);
                
                else if ( timeframe == 3 & weekday == 0 ) cp.y_night_monday = predict(op, amount_predictions);
                else if ( timeframe == 3 & weekday == 1 ) cp.y_night_tuesday = predict(op, amount_predictions);
                else if ( timeframe == 3 & weekday == 2 ) cp.y_night_wednesday = predict(op, amount_predictions);
                else if ( timeframe == 3 & weekday == 3 ) cp.y_night_thursday = predict(op, amount_predictions);
                else if ( timeframe == 3 & weekday == 4 ) cp.y_night_friday = predict(op, amount_predictions);
                else if ( timeframe == 3 & weekday == 5 ) cp.y_night_saturday = predict(op, amount_predictions);
                else if ( timeframe == 3 & weekday == 6 ) cp.y_night_sunday = predict(op, amount_predictions);
                
                vip.clear();
            }
        }
        
        crowdedness_predictions.push_back( cp );
        
        std::cout << "All crowdedness predictions for bar " << barID << " are completed.\n";
    }
    
    std::cout << std::endl;
    
    return EXIT_SUCCESS;
}


 */



// function to check if the input is correct or not
int checkInput ( const int & weekday, const int & timeframe, const int & barID,const int & DAYSTOPREDICT){
    
    if (weekday < 0 | weekday > 6)
    {
        std::cout << "You must specify a valid weekday." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    if (barID < 0)
    {
        std::cout << "You must specify a valid bar ID." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    if (timeframe < 0 | timeframe > 3)
    {
        std::cout << "You must specify a valid timeframe." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    if (DAYSTOPREDICT < 0 )
    {
        std::cout << "You must specify a valid DAYSTOPREDICT value, (remember at least 1)." << std::endl;
        
        return EXIT_FAILURE;
    }
    return 0;
}

