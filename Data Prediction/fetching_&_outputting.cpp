//
//  fetching_&_outputting.cpp


#include "fetching_&_outputting.hpp"

std::vector< BarData > historicalDataSet;
std::vector< processedData > processedDataSet;

template< typename T >
T parse_value(const std::__1::basic_string< char > * value)
{
    std::istringstream convert( *value );
    
    T result;
    
    return convert >> result ? result : INT_MAX;
}

template<>
std::string parse_value(const std::__1::basic_string< char > * value)
{
    std::istringstream convert( *value );
    
    std::string result;
    
    return convert >> result ? result : "";
}

template< typename T >
int fetch_data(const std::__1::basic_string< char > & value, const std::string & name, T * field)
{
    *field = parse_value< T >( &value );
    
    if ( *field == INT_MAX )
    {
        std::cout << "There was a problem fetching " << name << " from the .JSON file." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

template<>
int fetch_data(const std::__1::basic_string< char > & value, const std::string & name, std::vector< double > * vec)
{
    double crowdedness = parse_value< double >( & value );
    
    if ( crowdedness == INT_MAX )
    {
        std::cout << "There was a problem fetching " << name << " from the .JSON file." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    else vec->push_back( crowdedness );
    
    return EXIT_SUCCESS;
}

template<>
int fetch_data(const std::__1::basic_string< char > & value, const std::string & name, std::string * field)
{
    *field = parse_value< std::string >( &value );
    
    if ( *field == "" )
    {
        std::cout << "There was a problem fetching " << name << " from the .JSON file." << std::endl;
        
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

int fill_data(const boost::property_tree::ptree & root, const bool & VERBOSE, const bool & PARALLEL, const unsigned int & bars)
{
    /*  For this particular set of nested loops there is a very strong data dependency between scopes, that is, the data of the inner loop strongly depends on that of the outer loop, which makes it inadecuate to be parallelized. The way this has been solved, however, is by parallelizing ONLY the outer loop. The parallel loop has been scheduled statically because all threads will do a very similar amount of work. throughRoot is private because all threads will "take off" from different parts of the root. */
    
#pragma omp parallel if(PARALLEL)
    {
        // here we distribute equally the work that each thread will do
        unsigned int ID = omp_get_thread_num(),
        threads = omp_get_num_threads(),
        start = ID * (bars / threads),
        end = start + (bars / threads),
        count = 0;
        
        if ( VERBOSE )
        {
#pragma omp critical
            std::cout << "\n[fill data] thread " << ID << " from " << start << " to " << end << std::endl;
        }
        
        auto throughRoot = root.begin();
        
        /*  Each thread shall advance the iterator to the place where it will start fetchig data from. This way all threads can parallely access the same data structures safely since they'll access strictly different places of it. */
        
        for ( unsigned int i = 0; i < start; i++ ) throughRoot++;
        
        for ( unsigned int outter = start; outter < end; outter++ )
        {
            BarData *db = new BarData();
            
            count = 0;
            
            for ( auto throughBars = throughRoot->second.begin(); throughBars != throughRoot->second.end(); throughBars++ )
            {
                count++;
                
                if ( throughBars->first == "ID" )
                {
                    if ( fetch_data< unsigned int >( throughBars->second.data(), "ID", & db->ID ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Has_darts" )
                {
                    if ( fetch_data< unsigned int >( throughBars->second.data(), "Has_darts", & db->Has_darts ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Has_football" )
                {
                    if ( fetch_data< unsigned int >( throughBars->second.data(), "Has_football", & db->Has_football ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Has_pool" )
                {
                    if ( fetch_data< unsigned int >( throughBars->second.data(), "Has_pool", & db->Has_pool ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Beer_price" )
                {
                    if ( fetch_data< float >( throughBars->second.data(), "Beer_price", & db->Beer_price ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Cafe_price" )
                {
                    if ( fetch_data< float >( throughBars->second.data(), "Cafe_price", & db->Cafe_price ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Soda_price" )
                {
                    if ( fetch_data< float >( throughBars->second.data(), "Soda_price", & db->Soda_price ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Longitude" )
                {
                    if ( fetch_data< double >( throughBars->second.data(), "Longitude", & db->Longitude ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Latitude" )
                {
                    if ( fetch_data< double >( throughBars->second.data(), "Latitude", & db->Latitude ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Beer_ometer" )
                {
                    if ( fetch_data< unsigned int >( throughBars->second.data(), "Beer_ometer", & db->Beer_ometer ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBars->first == "Atmosphere" )
                {
                    if ( fetch_data< unsigned int >( throughBars->second.data(), "Atmosphere", & db->Atmosphere ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else
                {
                    if ( count % 4 == 0 )
                    {
                        if ( fetch_data( throughBars->second.data(), "Morning Crowdedness", & db->morning ) == EXIT_FAILURE )
                            exit(EXIT_FAILURE);
                    }
                    
                    else if ( count % 4 == 1 )
                    {
                        if ( fetch_data( throughBars->second.data(), "Midday Crowdedness", & db->midday ) == EXIT_FAILURE )
                            exit(EXIT_FAILURE);
                    }
                    
                    else if ( count % 4 == 2 )
                    {
                        if ( fetch_data( throughBars->second.data(), "Evening Crowdedness", & db->evening ) == EXIT_FAILURE )
                            exit(EXIT_FAILURE);
                    }
                    
                    else
                    {
                        if ( fetch_data( throughBars->second.data(), "Night Crowdedness", & db->late_night ) == EXIT_FAILURE )
                            exit(EXIT_FAILURE);
                    }
                }
            }
            
#pragma omp critical
            historicalDataSet.push_back( *db );
            
            throughRoot++;
            
            if ( VERBOSE ) std::cout << "Data from from bar " << db->ID << " successfully fetched.\n";
            
            delete db;
        }
    }
    
    std::cout << std::endl;
    
    return EXIT_SUCCESS;
}

int fill_processed_data(const boost::property_tree::ptree & root, const bool & VERBOSE, const bool & PARALLEL, const unsigned int & bars)
{
#pragma omp parallel if(PARALLEL)
    {
        // here we distribute equally the work that each thread will do
        unsigned int ID = omp_get_thread_num(),
        threads = omp_get_num_threads(),
        start = ID * (bars / threads),
        end = start + (bars / threads);
        
        if ( VERBOSE )
        {
#pragma omp critical
            std::cout << "\n[fill_processed_data] thread " << ID << " from " << start << " to " << end << std::endl;
        }
        
        auto throughRoot = root.begin();
        auto throughBarsOutter = throughRoot->second.begin();
        
        /*  Each thread shall advance the iterator to the place where it will start fetchig data from. This way all threads can parallely access the same data structures safely since they'll access strictly different places of it. */
        
        for ( unsigned int i = 0; i < start; i++ ) throughRoot++;
        
        for ( unsigned int outter = start; outter < end; outter++ )
        {
            processedData *db = new processedData();
            //iterate through all the bars and fetch each value : rating, atmosphere, football...
            for ( auto throughBarsInner = throughBarsOutter->second.begin(); throughBarsInner != throughBarsOutter->second.end(); throughBarsInner++ )
            {
                if ( throughBarsInner->first == "rating" )
                {
                    if ( fetch_data< unsigned int >( throughBarsInner->second.data(), "rating", & db->rating ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "atmosphere" )
                {
                    if ( fetch_data< unsigned int >( throughBarsInner->second.data(), "atmosphere", & db->atmosphere ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "football" )
                {
                    if ( fetch_data< unsigned int >( throughBarsInner->second.data(), "football", & db->football ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "neighborhood" )
                {
                    if ( fetch_data< std::string >( throughBarsInner->second.data(), "neighborhood", & db->neighborhood ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "name" )
                {
                    if ( fetch_data< std::string >( throughBarsInner->second.data(), "name", & db->name ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "zone" )
                {
                    if ( fetch_data< unsigned int >( throughBarsInner->second.data(), "zone", & db->zone ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "beerPrice" )
                {
                    if ( fetch_data< float >( throughBarsInner->second.data(), "beerPrice", & db->beerPrice ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "billiards" )
                {
                    if ( fetch_data< unsigned int >( throughBarsInner->second.data(), "billiards", & db->billiards ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                // this value is not needed since we don't worry about the previous values of predictions
                else if ( throughBarsInner->first == "prediction" )
                {
                    // do nothing
                    
                }
                
                else if ( throughBarsInner->first == "longitude" )
                {
                    if ( fetch_data< double >( throughBarsInner->second.data(), "longitude", & db->longitude ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "beerPrice_prediction" )
                {
                    if ( fetch_data< float >( throughBarsInner->second.data(), "beerPrice_prediction", & db->beerPrice_prediction ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "latitude" )
                {
                    if ( fetch_data< double >( throughBarsInner->second.data(), "latitude", & db->latitude ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "sodaPrice" )
                {
                    if ( fetch_data< float >( throughBarsInner->second.data(), "sodaPrice", & db->sodaPrice ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "url" )
                {
                    if ( fetch_data< std::string >( throughBarsInner->second.data(), "url", & db->url ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "darts" )
                {
                    if ( fetch_data< unsigned int >( throughBarsInner->second.data(), "darts", & db->darts ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "coffeePrice" )
                {
                    if ( fetch_data< float >( throughBarsInner->second.data(), "coffeePrice", & db->coffeePrice ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "prediction_rating" )
                {
                    if ( fetch_data< unsigned int >( throughBarsInner->second.data(), "prediction_rating", & db->prediction_rating ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "beerPrice_prediction_rating" )
                {
                    if ( fetch_data< float >( throughBarsInner->second.data(), "beerPrice_prediction_rating", & db->beerPrice_prediction_rating ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "beerPrice_rating" )
                {
                    if ( fetch_data< float >( throughBarsInner->second.data(), "beerPrice_rating", & db->beerPrice_rating ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
                
                else if ( throughBarsInner->first == "id" )
                {
                    if ( fetch_data< unsigned int >( throughBarsInner->second.data(), "id", & db->id ) == EXIT_FAILURE )
                        exit(EXIT_FAILURE);
                }
            }
            
#pragma omp critical
            processedDataSet.push_back( *db );
            
            throughBarsOutter++;
            
            if ( VERBOSE ) std::cout << "Preprocessed data from from bar " << db->id << " successfully fetched.\n";
            
            delete db;
        }
    }
    
    std::cout << std::endl;
    
    return EXIT_SUCCESS;
}

int output_predictions(boost::property_tree::ptree & output, const bool & PARALLEL, const bool & VERBOSE, const unsigned int & bars)
{
#pragma omp parallel if(PARALLEL)
    {
        auto v = processedDataSet.begin();
        
        // here we distribute equally the work that each thread will do
        unsigned int ID = omp_get_thread_num(),
        threads = omp_get_num_threads(),
        start = ID * (bars / threads),
        end = start + (bars / threads);
        
        if ( VERBOSE )
        {
#pragma omp critical
            std::cout << "\n[output_predictions] thread " << ID << " from " << start << " to " << end << std::endl;
        }
        
        /*  Each thread shall advance the iterator to the place where it will start fetchig data from. This way all threads can parallely access the same data structures safely since they'll access strictly different places of it. */
        
        for ( unsigned int i = 0; i < start; i++ ) v++;
        
        boost::property_tree::ptree array;
        
        for ( unsigned int i = start; i < end; i++ )
        {
            boost::property_tree::ptree bar;
            
            bar.put("rating", v->rating);
            bar.put("atmosphere", v->atmosphere);
            bar.put("football", v->football);
            bar.put("neighborhood", v->neighborhood);
            bar.put("name", v->name);
            bar.put("zone", v->zone);
            bar.put("beerPrice", v->beerPrice);
            bar.put("billiards", v->billiards);
            bar.put("prediction", v->prediction);
            bar.put("longitude", v->longitude);
            bar.put("beerPrice_prediction", v->beerPrice_prediction);
            bar.put("latitude", v->latitude);
            bar.put("sodaPrice", v->sodaPrice);
            bar.put("url", v->url);
            bar.put("darts", v->darts);
            bar.put("coffeePrice", v->coffeePrice);
            bar.put("prediction_rating", v->prediction_rating);
            bar.put("beerPrice_prediction_rating", v->beerPrice_prediction_rating);
            bar.put("beerPrice_rating", v->beerPrice_rating);
            bar.put("id", v->id);
            
#pragma omp critical
            array.push_back(std::make_pair("", bar));
            
            v++;
        }
        
#pragma omp critical
        output.add_child( "bars", array );
    }
    
    return EXIT_SUCCESS;
}
