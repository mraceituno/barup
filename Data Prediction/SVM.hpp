//
//  SVM.hpp
//  Distributed Project
//
//  Created by Arnau Oller on 27/05/2018.
//  Copyright © 2018 Arnau Oller. All rights reserved.
//

#ifndef SVM_hpp
#define SVM_hpp

#include <stdio.h>
#include "fetching_&_outputting.hpp"
#include <limits>
#include "fetching_&_outputtingSVM.hpp"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/ml.hpp>


int generate_input_array_svm(const int & weekday, const int & timeframe, const int & barID,const int & DAYSTOPREDICT);
int checkInput(const int & weekday, const int & timeframe, const int & barID,const int & DAYSTOPREDICT);



#endif /* SVM_hpp */
