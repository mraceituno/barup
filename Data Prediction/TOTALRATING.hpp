//
//  TOTALRATING.hpp
//  Distributed Project
//
//  Created by Arnau Oller on 28/05/2018.
//  Copyright © 2018 Arnau Oller. All rights reserved.
//

#ifndef TOTALRATING_hpp
#define TOTALRATING_hpp

#include <stdio.h>
#include "fetching_&_outputting.hpp"
#include <limits>
#include "fetching_&_outputtingSVM.hpp"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/ml.hpp>

//ARREGLAR
int generalPredictionSvm(const int & numBars,const int & darts,const int & football, const int & pool, const float &  beer, const float & cafe,const float & soda,const float & longitude,const float & latitude,const int & ometer,const int & Atmos);

int checkInput(const int & numBars,const int & darts,const int & football, const int & pool, const float &  beer, const float & cafe,const float & soda,const float & longitude,const float & latitude,const int & ometer,const int & Atmos);

#endif /* TOTALRATING_hpp */
