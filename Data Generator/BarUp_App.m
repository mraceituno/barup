clear

Number_of_Bars = 7400;

%ID
parfor i = 1:Number_of_Bars
   Data(i,1) = i; 
end

%Has_Darts
parfor i = 1:Number_of_Bars
   Data(i,2) =  round(rand);
end

%Has_football
parfor i = 1:Number_of_Bars
   Data(i,3) =  round(rand);
end

%Has_pool
parfor i = 1:Number_of_Bars
   Data(i,4) =  round(rand);
end

%Beer_price
parfor i = 1:Number_of_Bars
   Data(i,5) = (round(10*normrnd(1.50,0.30)))/10;
end

%Cafe_price
parfor i = 1:Number_of_Bars
   Data(i,6) =  (round(10*normrnd(1.30,0.20)))/10;
end

%Soda_price
parfor i = 1:Number_of_Bars
   Data(i,7) =  (round(10*normrnd(1.80,0.30)))/10;
end

%Longitude
parfor i = 1:Number_of_Bars
   format long
   r = 41.35165354629629 +(41.44333780463222 - 41.35165354629629).*rand(1,1);
   Data(i,8) = r;
   
end

%Latitude
parfor i = 1:Number_of_Bars
   format long
   r = 2.1049348708611433 +(2.203468501232237 - 2.1049348708611433).*rand(1,1);
   Data(i,9) = r;
end

%Beer'ometer
parfor i = 1:Number_of_Bars
   Data(i,10) =(round(5.*rand(1,1)));
end

%Atmosphere
parfor i = 1:Number_of_Bars
    %1 - Sport (Sports events on friday and sunday)
    %2 - Youth (Happy hour)
    %3 - Familiar
    %4 - Luxurious
   Data(i,11) =(round(3.*rand(1,1)));
end

%More real variables, all real
probability_of_festival = (1/120); %once per month
probability_of_closure = (180/7400); %one bar closes per day
probability_of_reopening = (360/7400); %bar reopenings (Chinesse most)
probability_of_fire =(22/7400); %bars that burn in Barcelona every 6 months
probability_of_bad_reputation = (100/7400); %amount of fights per nigth
probability_of_transmit_big_sport_event = (1/3); %probability that a bar that is not sport atmosphere transmit a match


%Crowdedness level
for i = 1:Number_of_Bars
    disp(i);
    for j = 12:731
        if(mod(j-12,28) == 0 || mod(j-12,28) == 1 || mod(j-12,28) == 2 || mod(j-12,28) == 3 )%Monday
            if(mod(j-12,28) == 0)%Morning
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    %Marina is closed ALL mondays
                    Data(i,j)= 0.0001; %To distinguish from closure 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.3,0.05)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    random = rand();
                    if (random > probability_of_reopening)
                        Data(i,j) = 0;
                    end
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    %Marina is closed ALL mondays
                    Data(i,j)= 0.0001; %To distinguish from closure
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end          
                
            elseif(mod(j-12,28) == 1)%Midday
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    %Marina is closed ALL mondays
                    Data(i,j)= 0.0001; %To distinguish from closure
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.45,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.3,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    %Marina is closed ALL mondays
                    Data(i,j)= 0.0001; %To distinguish from closure
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end 
                
                
            elseif(mod(j-12,28) == 2)%Evening
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    %Marina is closed ALL mondays
                    Data(i,j)= 0.0001; %To distinguish from closure
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.55,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.5,0.1)).^2));
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    %Marina is closed ALL mondays
                    Data(i,j)= 0.0001; %To distinguish from closure
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
                
                
            elseif(mod(j-12,28) == 3)%Late Night
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    %Marina is closed ALL mondays
                    Data(i,j)= 0.0001; %To distinguish from closure
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.15,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.25;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                random = rand();
                if(random < probability_of_transmit_big_sport_event)
                    Data(i,j) = Data(i,j) + 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    %Marina is closed ALL mondays
                    Data(i,j)= 0.0001; %To distinguish from closure
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
            end
        end
         if(mod(j-12,28) == 4 || mod(j-12,28) == 5 || mod(j-12,28) == 6 || mod(j-12,28) == 7)%Tuesday
            if(mod(j-12,28) == 4)%Morning
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.1,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.3,0.05)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    random = rand();
                    if (random > probability_of_reopening)
                        Data(i,j) = 0;
                    end
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end          
                
            elseif(mod(j-12,28) == 5)%Midday
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.2,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.45,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.3,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end 
                
                
            elseif(mod(j-12,28) == 6)%Evening
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.6,0.05)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.55,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.5,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
                
                
            elseif(mod(j-12,28) == 7)%Late Night
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.4,0.05)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.15,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                random = rand();
                if(random < probability_of_transmit_big_sport_event)
                    Data(i,j) = Data(i,j) + 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
            end
         end
         if(mod(j-12,28) == 8 || mod(j-12,28) == 9 || mod(j-12,28) == 10 || mod(j-12,28) == 11 )%Wednesday
            if(mod(j-12,28) == 8)%Morning
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.1,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.3,0.05)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    random = rand();
                    if (random > probability_of_reopening)
                        Data(i,j) = 0;
                    end
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end          
                
            elseif(mod(j-12,28) == 9)%Midday
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.2,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.45,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.3,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end 
                
                
            elseif(mod(j-12,28) == 10)%Evening
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.6,0.05)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.55,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.5,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
                
                
            elseif(mod(j-12,28) == 11)%Late Night
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.3,0.05)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                random = rand();
                if(random < probability_of_transmit_big_sport_event)
                    Data(i,j) = Data(i,j) + 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
            end
         end
        if(mod(j-12,28) == 12 || mod(j-12,28) ==13 || mod(j-12,28) == 14 || mod(j-12,28) == 15)%Thursday
            if(mod(j-12,28) == 12)%Morning
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.1,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.3,0.05)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    random = rand();
                    if (random > probability_of_reopening)
                        Data(i,j) = 0;
                    end
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end          
                
            elseif(mod(j-12,28) == 13)%Midday
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.2,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.45,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.3,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end 
                
            elseif(mod(j-12,28) == 14)%Evening
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.6,0.05)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.55,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.5,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
                
            elseif(mod(j-12,28) == 15)%Late Night
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.3,0.05)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                random = rand();
                if(random < probability_of_transmit_big_sport_event)
                    Data(i,j) = Data(i,j) + 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
            end
        end
        if(mod(j-12,28) == 16 || mod(j-12,28) == 17 || mod(j-12,28) == 18 || mod(j-12,28) == 19 )%Friday
            if(mod(j-12,28) == 16)%Morning
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.2,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.3,0.05)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.2,0.1)).^2));
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    random = rand();
                    if (random > probability_of_reopening)
                        Data(i,j) = 0;
                    end
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end          
                
            elseif(mod(j-12,28) == 17)%Midday
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.35,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.45,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.3,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end 
                
                
            elseif(mod(j-12,28) == 18)%Evening
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.75,0.1)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.55,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.6,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_transmit_big_sport_event)
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
                
                
            elseif(mod(j-12,28) == 19)%Late Night
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.8,0.05)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.7,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.75,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                random = rand();
                if(random < probability_of_transmit_big_sport_event)
                    Data(i,j) = Data(i,j) + 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
            end
        end
        if(mod(j-12,28) == 20 || mod(j-12,28) == 21 || mod(j-12,28) == 22 || mod(j-12,28) == 23)%Saturday
            if(mod(j-12,28) == 20)%Morning
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=0.0001;
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.35,0.05)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.4,0.1)).^2));
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    random = rand();
                    if (random > probability_of_reopening)
                        Data(i,j) = 0;
                    end
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end          
                
            elseif(mod(j-12,28) == 21)%Midday
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.55,0.05)).^2)); 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.5,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.45,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.05;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end 
                
                
            elseif(mod(j-12,28) == 22)%Evening
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.8,0.1)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.7,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.8,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.2;
                end
                random = rand();
                if(random < probability_of_transmit_big_sport_event)
                    Data(i,j) = Data(i,j) + 0.15;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
                
                
            elseif(mod(j-12,28) == 23)%Late Night
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111) 
                    %Zone: Marina
                    Data(i,j)=(sqrt((normrnd(0.9,0.05)).^2));
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=(sqrt((normrnd(0.75,0.1)).^2));
                else
                    %Other zones:
                    Data(i,j)=(sqrt((normrnd(0.8,0.1)).^2));
                end
                if(Data(i,2)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,3)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,4)== 1)
                    Data(i,j) = Data(i,j)+0.10;
                end
                if(Data(i,11) == 1) %Sports type
                    Data(i,j) = Data(i,j) + 0.2;
                end
                random = rand();
                if(random < probability_of_festival)
                    Data(i,j) = Data(i,j) + 0.10;
                end
                random = rand();
                if(random < probability_of_bad_reputation)
                    Data(i,j) = Data(i,j) - 0.15;
                end
                random = rand();
                if(random < probability_of_transmit_big_sport_event)
                    Data(i,j) = Data(i,j) + 0.15;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                random = rand();
                if(random < probability_of_fire)
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
            end
        end
        if(mod(j-12,28) == 24 || mod(j-12,28) == 25 || mod(j-12,28) == 26 || mod(j-12,28) == 27)%Sunday
            if(mod(j-12,28) == 24)%Morning
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=0.0001; 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=0.0001;
                else
                    %Other zones:
                    Data(i,j)=0.0001;
                end
                if (Data(i,j-1) == 0)%It's closed
                    random = rand();
                    if (random > probability_of_reopening)
                        Data(i,j) = 0;
                    end
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end          
                
            elseif(mod(j-12,28) == 25)%Midday
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=0.0001; 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=0.0001;
                else
                    %Other zones:
                    Data(i,j)=0.0001;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
            elseif(mod(j-12,28) == 26)%Evening
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=0.0001; 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=0.0001;
                else
                    %Other zones:
                    Data(i,j)=0.0001;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
            elseif(mod(j-12,28) == 27)%Late Night
                if(Data(i,8) > 41.395042 && Data(i,8) < 41.401384 && Data(i,9) > 2.191972 && Data(i,9) < 2.192111)
                    %Zone: Marina
                    Data(i,j)=0.0001; 
                elseif(Data(i,8) > 41.373658 && Data(i,8) < 41.376949 && Data(i,9) > 2.170381 && Data(i,9) < 2.170817)
                    %Zone: Paralel
                    Data(i,j)=0.0001;
                else
                    %Other zones:
                    Data(i,j)=0.0001;
                end
                if (Data(i,j-1) == 0)%It's closed
                    Data(i,j) = 0;
                end
                if Data(i,j) > 1
                    Data(i,j) = 1;
                end
                if Data(i,j) < 0
                    Data(i,j) = 0.0001;
                end
                
            end
        end
    end
end

%Zone
for i = 1:Number_of_Bars
    if(Data(i,8) > 41.35 && Data(i,8) < 41.37 && Data(i,9) > 2.10 && Data(i,9) < 2.16)
    %Zone: Sants-Montjuic
    Data(i,732) = 1;
    elseif(Data(i,8) > 41.35 && Data(i,8) < 41.37 && Data(i,9) > 2.16 && Data(i,9) < 2.20)
    %Zone: Ciutat Vella I
    Data(i,732) = 2;
    elseif(Data(i,8) > 41.37 && Data(i,8) < 41.39 && Data(i,9) > 2.15 && Data(i,9) < 2.20)
    %Zone: Ciutat Vella II
    Data(i,732) = 2;
    elseif(Data(i,8) > 41.37 && Data(i,8) < 41.40 && Data(i,9) > 2.10 && Data(i,9) < 2.14)
    %Zone: Les Corts
    Data(i,732) = 3;
    elseif(Data(i,8) > 41.39 && Data(i,8) < 41.41 && Data(i,9) > 2.17 && Data(i,9) < 2.20)
    %Zone: Sant Mart�
    Data(i,732) = 4;
    elseif(Data(i,8) > 41.40 && Data(i,8) < 41.42 && Data(i,9) > 2.13 && Data(i,9) < 2.15)
    %Zone: Gracia
    Data(i,732) = 5;
    elseif(Data(i,8) > 41.40 && Data(i,8) < 41.42 && Data(i,9) > 2.10 && Data(i,9) < 2.13)
    %Zone: Sarria
    Data(i,732) = 6;
    elseif(Data(i,8) > 41.42 && Data(i,8) < 41.44 && Data(i,9) > 2.10 && Data(i,9) < 2.15)
    %Zone: Horta-Guinardo
    Data(i,732) = 7;
    elseif(Data(i,8) > 41.42 && Data(i,8) < 41.44 && Data(i,9) > 2.15 && Data(i,9) < 2.17)
    %Zone: Nou Barris
    Data(i,732) = 8;
    elseif(Data(i,8) > 41.41 && Data(i,8) < 41.44 && Data(i,9) > 2.17 && Data(i,9) < 2.20)
    %Zone: Sant Andreu
    Data(i,732) = 9;
    else
    %Zone: Eixample
    Data(i,732) = 10;
    end
end

%url
parfor i = 1:Number_of_Bars
    Data(i,733) = (round(19.*rand(1,1)));
end

for i = 1:Number_of_Bars
    for j = 1:11
        Data_App(i,j) = Data(i,j);
    end
    Data_App(i,12) = Data(i,732);
    Data_App(i,13) = Data(i,733);
    Data_App(i,14) = (round(2.*rand(1,1)));
end
%%
plotmatrix(Data(:,8),Data(:,9))
%%
T = array2table(Data,...
    'VariableNames',{'ID','Has_darts','Has_football','Has_pool','Beer_price','Cafe_price','Soda_price','Longitude','Latitude','Beer_ometer','Atmosphere','Day_1_Morning','Day_1_Midday','Day_1_Evening','Day_1_Late_Night','Day_2_Morning','Day_2_Midday','Day_2_Evening','Day_2_Late_Night','Day_3_Morning','Day_3_Midday','Day_3_Evening','Day_3_Late_Night','Day_4_Morning','Day_4_Midday','Day_4_Evening','Day_4_Late_Night','Day_5_Morning','Day_5_Midday','Day_5_Evening','Day_5_Late_Night','Day_6_Morning','Day_6_Midday','Day_6_Evening','Day_6_Late_Night','Day_7_Morning','Day_7_Midday','Day_7_Evening','Day_7_Late_Night','Day_8_Morning','Day_8_Midday','Day_8_Evening','Day_8_Late_Night','Day_9_Morning','Day_9_Midday','Day_9_Evening','Day_9_Late_Night','Day_10_Morning','Day_10_Midday','Day_10_Evening','Day_10_Late_Night','Day_11_Morning','Day_11_Midday','Day_11_Evening','Day_11_Late_Night','Day_12_Morning','Day_12_Midday','Day_12_Evening','Day_12_Late_Night','Day_13_Morning','Day_13_Midday','Day_13_Evening','Day_13_Late_Night','Day_14_Morning','Day_14_Midday','Day_14_Evening','Day_14_Late_Night','Day_15_Morning','Day_15_Midday','Day_15_Evening','Day_15_Late_Night','Day_16_Morning','Day_16_Midday','Day_16_Evening','Day_16_Late_Night','Day_17_Morning','Day_17_Midday','Day_17_Evening','Day_17_Late_Night','Day_18_Morning','Day_18_Midday','Day_18_Evening','Day_18_Late_Night','Day_19_Morning','Day_19_Midday','Day_19_Evening','Day_19_Late_Night','Day_20_Morning','Day_20_Midday','Day_20_Evening','Day_20_Late_Night','Day_21_Morning','Day_21_Midday','Day_21_Evening','Day_21_Late_Night','Day_22_Morning','Day_22_Midday','Day_22_Evening','Day_22_Late_Night','Day_23_Morning','Day_23_Midday','Day_23_Evening','Day_23_Late_Night','Day_24_Morning','Day_24_Midday','Day_24_Evening','Day_24_Late_Night','Day_25_Morning','Day_25_Midday','Day_25_Evening','Day_25_Late_Night','Day_26_Morning','Day_26_Midday','Day_26_Evening','Day_26_Late_Night','Day_27_Morning','Day_27_Midday','Day_27_Evening','Day_27_Late_Night','Day_28_Morning','Day_28_Midday','Day_28_Evening','Day_28_Late_Night','Day_29_Morning','Day_29_Midday','Day_29_Evening','Day_29_Late_Night','Day_30_Morning','Day_30_Midday','Day_30_Evening','Day_30_Late_Night','Day_31_Morning','Day_31_Midday','Day_31_Evening','Day_31_Late_Night','Day_32_Morning','Day_32_Midday','Day_32_Evening','Day_32_Late_Night','Day_33_Morning','Day_33_Midday','Day_33_Evening','Day_33_Late_Night','Day_34_Morning','Day_34_Midday','Day_34_Evening','Day_34_Late_Night','Day_35_Morning','Day_35_Midday','Day_35_Evening','Day_35_Late_Night','Day_36_Morning','Day_36_Midday','Day_36_Evening','Day_36_Late_Night','Day_37_Morning','Day_37_Midday','Day_37_Evening','Day_37_Late_Night','Day_38_Morning','Day_38_Midday','Day_38_Evening','Day_38_Late_Night','Day_39_Morning','Day_39_Midday','Day_39_Evening','Day_39_Late_Night','Day_40_Morning','Day_40_Midday','Day_40_Evening','Day_40_Late_Night','Day_41_Morning','Day_41_Midday','Day_41_Evening','Day_41_Late_Night','Day_42_Morning','Day_42_Midday','Day_42_Evening','Day_42_Late_Night','Day_43_Morning','Day_43_Midday','Day_43_Evening','Day_43_Late_Night','Day_44_Morning','Day_44_Midday','Day_44_Evening','Day_44_Late_Night','Day_45_Morning','Day_45_Midday','Day_45_Evening','Day_45_Late_Night','Day_46_Morning','Day_46_Midday','Day_46_Evening','Day_46_Late_Night','Day_47_Morning','Day_47_Midday','Day_47_Evening','Day_47_Late_Night','Day_48_Morning','Day_48_Midday','Day_48_Evening','Day_48_Late_Night','Day_49_Morning','Day_49_Midday','Day_49_Evening','Day_49_Late_Night','Day_50_Morning','Day_50_Midday','Day_50_Evening','Day_50_Late_Night','Day_51_Morning','Day_51_Midday','Day_51_Evening','Day_51_Late_Night','Day_52_Morning','Day_52_Midday','Day_52_Evening','Day_52_Late_Night','Day_53_Morning','Day_53_Midday','Day_53_Evening','Day_53_Late_Night','Day_54_Morning','Day_54_Midday','Day_54_Evening','Day_54_Late_Night','Day_55_Morning','Day_55_Midday','Day_55_Evening','Day_55_Late_Night','Day_56_Morning','Day_56_Midday','Day_56_Evening','Day_56_Late_Night','Day_57_Morning','Day_57_Midday','Day_57_Evening','Day_57_Late_Night','Day_58_Morning','Day_58_Midday','Day_58_Evening','Day_58_Late_Night','Day_59_Morning','Day_59_Midday','Day_59_Evening','Day_59_Late_Night','Day_60_Morning','Day_60_Midday','Day_60_Evening','Day_60_Late_Night','Day_61_Morning','Day_61_Midday','Day_61_Evening','Day_61_Late_Night','Day_62_Morning','Day_62_Midday','Day_62_Evening','Day_62_Late_Night','Day_63_Morning','Day_63_Midday','Day_63_Evening','Day_63_Late_Night','Day_64_Morning','Day_64_Midday','Day_64_Evening','Day_64_Late_Night','Day_65_Morning','Day_65_Midday','Day_65_Evening','Day_65_Late_Night','Day_66_Morning','Day_66_Midday','Day_66_Evening','Day_66_Late_Night','Day_67_Morning','Day_67_Midday','Day_67_Evening','Day_67_Late_Night','Day_68_Morning','Day_68_Midday','Day_68_Evening','Day_68_Late_Night','Day_69_Morning','Day_69_Midday','Day_69_Evening','Day_69_Late_Night','Day_70_Morning','Day_70_Midday','Day_70_Evening','Day_70_Late_Night','Day_71_Morning','Day_71_Midday','Day_71_Evening','Day_71_Late_Night','Day_72_Morning','Day_72_Midday','Day_72_Evening','Day_72_Late_Night','Day_73_Morning','Day_73_Midday','Day_73_Evening','Day_73_Late_Night','Day_74_Morning','Day_74_Midday','Day_74_Evening','Day_74_Late_Night','Day_75_Morning','Day_75_Midday','Day_75_Evening','Day_75_Late_Night','Day_76_Morning','Day_76_Midday','Day_76_Evening','Day_76_Late_Night','Day_77_Morning','Day_77_Midday','Day_77_Evening','Day_77_Late_Night','Day_78_Morning','Day_78_Midday','Day_78_Evening','Day_78_Late_Night','Day_79_Morning','Day_79_Midday','Day_79_Evening','Day_79_Late_Night','Day_80_Morning','Day_80_Midday','Day_80_Evening','Day_80_Late_Night','Day_81_Morning','Day_81_Midday','Day_81_Evening','Day_81_Late_Night','Day_82_Morning','Day_82_Midday','Day_82_Evening','Day_82_Late_Night','Day_83_Morning','Day_83_Midday','Day_83_Evening','Day_83_Late_Night','Day_84_Morning','Day_84_Midday','Day_84_Evening','Day_84_Late_Night','Day_85_Morning','Day_85_Midday','Day_85_Evening','Day_85_Late_Night','Day_86_Morning','Day_86_Midday','Day_86_Evening','Day_86_Late_Night','Day_87_Morning','Day_87_Midday','Day_87_Evening','Day_87_Late_Night','Day_88_Morning','Day_88_Midday','Day_88_Evening','Day_88_Late_Night','Day_89_Morning','Day_89_Midday','Day_89_Evening','Day_89_Late_Night','Day_90_Morning','Day_90_Midday','Day_90_Evening','Day_90_Late_Night','Day_91_Morning','Day_91_Midday','Day_91_Evening','Day_91_Late_Night','Day_92_Morning','Day_92_Midday','Day_92_Evening','Day_92_Late_Night','Day_93_Morning','Day_93_Midday','Day_93_Evening','Day_93_Late_Night','Day_94_Morning','Day_94_Midday','Day_94_Evening','Day_94_Late_Night','Day_95_Morning','Day_95_Midday','Day_95_Evening','Day_95_Late_Night','Day_96_Morning','Day_96_Midday','Day_96_Evening','Day_96_Late_Night','Day_97_Morning','Day_97_Midday','Day_97_Evening','Day_97_Late_Night','Day_98_Morning','Day_98_Midday','Day_98_Evening','Day_98_Late_Night','Day_99_Morning','Day_99_Midday','Day_99_Evening','Day_99_Late_Night','Day_100_Morning','Day_100_Midday','Day_100_Evening','Day_100_Late_Night','Day_101_Morning','Day_101_Midday','Day_101_Evening','Day_101_Late_Night','Day_102_Morning','Day_102_Midday','Day_102_Evening','Day_102_Late_Night','Day_103_Morning','Day_103_Midday','Day_103_Evening','Day_103_Late_Night','Day_104_Morning','Day_104_Midday','Day_104_Evening','Day_104_Late_Night','Day_105_Morning','Day_105_Midday','Day_105_Evening','Day_105_Late_Night','Day_106_Morning','Day_106_Midday','Day_106_Evening','Day_106_Late_Night','Day_107_Morning','Day_107_Midday','Day_107_Evening','Day_107_Late_Night','Day_108_Morning','Day_108_Midday','Day_108_Evening','Day_108_Late_Night','Day_109_Morning','Day_109_Midday','Day_109_Evening','Day_109_Late_Night','Day_110_Morning','Day_110_Midday','Day_110_Evening','Day_110_Late_Night','Day_111_Morning','Day_111_Midday','Day_111_Evening','Day_111_Late_Night','Day_112_Morning','Day_112_Midday','Day_112_Evening','Day_112_Late_Night','Day_113_Morning','Day_113_Midday','Day_113_Evening','Day_113_Late_Night','Day_114_Morning','Day_114_Midday','Day_114_Evening','Day_114_Late_Night','Day_115_Morning','Day_115_Midday','Day_115_Evening','Day_115_Late_Night','Day_116_Morning','Day_116_Midday','Day_116_Evening','Day_116_Late_Night','Day_117_Morning','Day_117_Midday','Day_117_Evening','Day_117_Late_Night','Day_118_Morning','Day_118_Midday','Day_118_Evening','Day_118_Late_Night','Day_119_Morning','Day_119_Midday','Day_119_Evening','Day_119_Late_Night','Day_120_Morning','Day_120_Midday','Day_120_Evening','Day_120_Late_Night','Day_121_Morning','Day_121_Midday','Day_121_Evening','Day_121_Late_Night','Day_122_Morning','Day_122_Midday','Day_122_Evening','Day_122_Late_Night','Day_123_Morning','Day_123_Midday','Day_123_Evening','Day_123_Late_Night','Day_124_Morning','Day_124_Midday','Day_124_Evening','Day_124_Late_Night','Day_125_Morning','Day_125_Midday','Day_125_Evening','Day_125_Late_Night','Day_126_Morning','Day_126_Midday','Day_126_Evening','Day_126_Late_Night','Day_127_Morning','Day_127_Midday','Day_127_Evening','Day_127_Late_Night','Day_128_Morning','Day_128_Midday','Day_128_Evening','Day_128_Late_Night','Day_129_Morning','Day_129_Midday','Day_129_Evening','Day_129_Late_Night','Day_130_Morning','Day_130_Midday','Day_130_Evening','Day_130_Late_Night','Day_131_Morning','Day_131_Midday','Day_131_Evening','Day_131_Late_Night','Day_132_Morning','Day_132_Midday','Day_132_Evening','Day_132_Late_Night','Day_133_Morning','Day_133_Midday','Day_133_Evening','Day_133_Late_Night','Day_134_Morning','Day_134_Midday','Day_134_Evening','Day_134_Late_Night','Day_135_Morning','Day_135_Midday','Day_135_Evening','Day_135_Late_Night','Day_136_Morning','Day_136_Midday','Day_136_Evening','Day_136_Late_Night','Day_137_Morning','Day_137_Midday','Day_137_Evening','Day_137_Late_Night','Day_138_Morning','Day_138_Midday','Day_138_Evening','Day_138_Late_Night','Day_139_Morning','Day_139_Midday','Day_139_Evening','Day_139_Late_Night','Day_140_Morning','Day_140_Midday','Day_140_Evening','Day_140_Late_Night','Day_141_Morning','Day_141_Midday','Day_141_Evening','Day_141_Late_Night','Day_142_Morning','Day_142_Midday','Day_142_Evening','Day_142_Late_Night','Day_143_Morning','Day_143_Midday','Day_143_Evening','Day_143_Late_Night','Day_144_Morning','Day_144_Midday','Day_144_Evening','Day_144_Late_Night','Day_145_Morning','Day_145_Midday','Day_145_Evening','Day_145_Late_Night','Day_146_Morning','Day_146_Midday','Day_146_Evening','Day_146_Late_Night','Day_147_Morning','Day_147_Midday','Day_147_Evening','Day_147_Late_Night','Day_148_Morning','Day_148_Midday','Day_148_Evening','Day_148_Late_Night','Day_149_Morning','Day_149_Midday','Day_149_Evening','Day_149_Late_Night','Day_150_Morning','Day_150_Midday','Day_150_Evening','Day_150_Late_Night','Day_151_Morning','Day_151_Midday','Day_151_Evening','Day_151_Late_Night','Day_152_Morning','Day_152_Midday','Day_152_Evening','Day_152_Late_Night','Day_153_Morning','Day_153_Midday','Day_153_Evening','Day_153_Late_Night','Day_154_Morning','Day_154_Midday','Day_154_Evening','Day_154_Late_Night','Day_155_Morning','Day_155_Midday','Day_155_Evening','Day_155_Late_Night','Day_156_Morning','Day_156_Midday','Day_156_Evening','Day_156_Late_Night','Day_157_Morning','Day_157_Midday','Day_157_Evening','Day_157_Late_Night','Day_158_Morning','Day_158_Midday','Day_158_Evening','Day_158_Late_Night','Day_159_Morning','Day_159_Midday','Day_159_Evening','Day_159_Late_Night','Day_160_Morning','Day_160_Midday','Day_160_Evening','Day_160_Late_Night','Day_161_Morning','Day_161_Midday','Day_161_Evening','Day_161_Late_Night','Day_162_Morning','Day_162_Midday','Day_162_Evening','Day_162_Late_Night','Day_163_Morning','Day_163_Midday','Day_163_Evening','Day_163_Late_Night','Day_164_Morning','Day_164_Midday','Day_164_Evening','Day_164_Late_Night','Day_165_Morning','Day_165_Midday','Day_165_Evening','Day_165_Late_Night','Day_166_Morning','Day_166_Midday','Day_166_Evening','Day_166_Late_Night','Day_167_Morning','Day_167_Midday','Day_167_Evening','Day_167_Late_Night','Day_168_Morning','Day_168_Midday','Day_168_Evening','Day_168_Late_Night','Day_169_Morning','Day_169_Midday','Day_169_Evening','Day_169_Late_Night','Day_170_Morning','Day_170_Midday','Day_170_Evening','Day_170_Late_Night','Day_171_Morning','Day_171_Midday','Day_171_Evening','Day_171_Late_Night','Day_172_Morning','Day_172_Midday','Day_172_Evening','Day_172_Late_Night','Day_173_Morning','Day_173_Midday','Day_173_Evening','Day_173_Late_Night','Day_174_Morning','Day_174_Midday','Day_174_Evening','Day_174_Late_Night','Day_175_Morning','Day_175_Midday','Day_175_Evening','Day_175_Late_Night','Day_176_Morning','Day_176_Midday','Day_176_Evening','Day_176_Late_Night','Day_177_Morning','Day_177_Midday','Day_177_Evening','Day_177_Late_Night','Day_178_Morning','Day_178_Midday','Day_178_Evening','Day_178_Late_Night','Day_179_Morning','Day_179_Midday','Day_179_Evening','Day_179_Late_Night','Day_180_Morning','Day_180_Midday','Day_180_Evening','Day_180_Late_Night', 'Zone', 'url'});
T_App = array2table(Data_App,... 
    'VariableNames',{'id','darts','football','billiards','beerPrice','coffeePrice','sodaPrice','longitude','latitude','rating','atmosphere','zone','url','prediction'});
%%
json = jsonencode(T);
FileID = fopen('BarData_Prediction.txt','w');
fprintf(FileID,'%s',json);
fclose(FileID);
json = jsonencode(T_App);
FileID = fopen('BarData_App.txt','w');
fprintf(FileID,'%s',json);
fclose(FileID);