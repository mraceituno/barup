# README #

This repository contains all the data, files and components used for developing the BarUp mobile app project.

This project consists of a mobile application that provides interesting, clear and reliable information about bars and pubs around the city and the prediction of how crowded a bar can be at the morning, midday, afternoon or night depending on the time frame you are at this moment.

[Demo](https://www.youtube.com/watch?v=y6hSIcym1-I&feature=youtu.be)

Further explanations are done in the Wiki of the repository. 

The repository contains all the generation and prediction work in the **master** branch.
Server files in **server** branch.
Mobile app files in **mobile** branch.

The app is also available to be downloaded using the expo launcher app:

* **Android:** In the explorer page, insert in the search bar @barupTest/barup

* **iOS:** log in as user: **barupTest** password: **baruppass**